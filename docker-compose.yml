version: "2.1"

# Inspired in: http://davidanguita.name/articles/dockerizing-a-phoenix-project/

services:

  workspace:
    image: tianon/true
    volumes:
      - ${APP_CODE_VOLUME_MAP:-./app:/home/elixir/workspace}

  elixir-iex:
    build: ./docker/build/elixir/${ELIXIR_VERSION:-1.6}
    image: ${ELIXIR_IMAGE:-exadra37/elixir}:${ELIXIR_VERSION:-1.6}
    env_file:
      - ${DATABASE_DEV_SECRETS_FILE:-secrets/database/dev/postgres/.env}
      - ${APP_DEV_SECRETS_FILE:-secrets/app/dev/.env}
    environment:
      - MIX_ENV=dev
      - DISPLAY=${DISPLAY:-:0.0}
    ports:
      - ${SHELL_PORTS_MAP:-4001:4000}
    working_dir: /home/elixir/workspace/${APP_NAME}
    command:
      - iex
      - -S
      - mix
    volumes:
      - /tmp/.X11-unix:/tmp/.X11-unix
    volumes_from:
      - workspace

  phoenix-iex:
    build: ./docker/build/phoenix/${PHOENIX_VERSION:-1.3}
    image: ${PHOENIX_IMAGE:-exadra37/phoenix}:${PHOENIX_VERSION:-1.3}
    env_file:
      - ${DATABASE_DEV_SECRETS_FILE:-secrets/database/dev/postgres/.env}
      - ${APP_DEV_SECRETS_FILE:-secrets/app/dev/.env}
    environment:
      - MIX_ENV=dev
      - DISPLAY=${DISPLAY:-:0.0}
    ports:
      - ${SHELL_PORTS_MAP:-4002:4000}
    working_dir: /home/elixir/workspace/${APP_NAME}
    command:
      - iex
      - -S
      - mix
    volumes:
      - /tmp/.X11-unix:/tmp/.X11-unix
    volumes_from:
      - workspace

  elixir-shell:
    build: ./docker/build/elixir/${ELIXIR_VERSION:-1.6}
    image: ${ELIXIR_IMAGE:-exadra37/elixir}:${ELIXIR_VERSION:-1.6}
    env_file:
      - ${DATABASE_DEV_SECRETS_FILE:-secrets/database/dev/postgres/.env}
      - ${APP_DEV_SECRETS_FILE:-secrets/app/dev/.env}
    environment:
      - MIX_ENV=dev
      - DISPLAY=${DISPLAY:-:0.0}
    depends_on:
      database-dev:
        condition: service_healthy
      database-test:
        condition: service_healthy
    ports:
      - ${SHELL_PORTS_MAP:-4003:4000}
    command:
      - zsh
    volumes_from:
      - workspace
    volumes:
      - /tmp/.X11-unix:/tmp/.X11-unix
      - ${SHELL_SSH_VOLUME_MAP:-~/.ssh:/home/elixir/.ssh}
      - ${SHELL_GIT_CONFIG_VOLUME_MAP:-~/.gitconfig:/home/elixir/.gitconfig}

  phoenix-shell:
    build: ./docker/build/phoenix/${PHOENIX_VERSION:-1.3}
    image: ${PHOENIX_IMAGE:-exadra37/phoenix}:${PHOENIX_VERSION:-1.3}
    env_file:
      - ${DATABASE_DEV_SECRETS_FILE:-secrets/database/dev/postgres/.env}
      - ${APP_DEV_SECRETS_FILE:-secrets/app/dev/.env}
    environment:
      - MIX_ENV=dev
    depends_on:
      database-dev:
        condition: service_healthy
      database-test:
        condition: service_healthy
    ports:
      - ${SHELL_PORTS_MAP:-4004:4000}
    command:
      - zsh
    volumes_from:
      - workspace
    volumes:
      - /tmp/.X11-unix:/tmp/.X11-unix
      - ${SHELL_SSH_VOLUME_MAP:-~/.ssh:/home/elixir/.ssh}
      - ${SHELL_GIT_CONFIG_VOLUME_MAP:-~/.gitconfig:/home/elixir/.gitconfig}

  elixir-http-dev:
    build: ./docker/build/elixir/${ELIXIR_VERSION:-1.6}
    image: ${ELIXIR_IMAGE:-exadra37/elixir}:${ELIXIR_VERSION:-1.6}
    env_file:
      - ${DATABASE_DEV_SECRETS_FILE:-secrets/database/dev/postgres/.env}
      - ${APP_DEV_SECRETS_FILE:-secrets/app/dev/.env}
    environment:
      - MIX_ENV=dev
    depends_on:
      database-dev:
        condition: service_healthy
    volumes_from:
      - workspace
    ports:
      - ${WEB_PORTS_MAP:-4005:4000}
    working_dir: /home/elixir/workspace/${APP_NAME}
    command:
      - mix

  phoenix-http-dev:
    build: ./docker/build/phoenix/${PHOENIX_VERSION:-1.3}
    image: ${PHOENIX_IMAGE:-exadra37/phoenix}:${PHOENIX_VERSION:-1.3}
    env_file:
      - ${DATABASE_DEV_SECRETS_FILE:-secrets/database/dev/postgres/.env}
      - ${APP_DEV_SECRETS_FILE:-secrets/app/dev/.env}
    environment:
      - MIX_ENV=dev
    depends_on:
      database-dev:
        condition: service_healthy
    volumes_from:
      - workspace
    ports:
      - ${WEB_PORTS_MAP:-4006:4000}
    working_dir: /home/elixir/workspace/${APP_NAME}
    command:
      - mix
      - phoenix.server

  database-prod:
    image: ${DATABASE_IMAGE:-postgres:10-alpine}
    env_file: ${DATABASE_PROD_SECRETS_FILE:-secrets/database/prod/postgres/.env}
    healthcheck:
      test: ["CMD-SHELL", "psql -h 'localhost' -U 'postgres' -c '\\l'"]
      interval: 2s
      timeout: 2s
      retries: 3
    ports:
      - "5432"
    volumes:
      - ~/.elixir/${APP_NAME}/database/prod/postgresql/data:/var/lib/postgresql/data

  database-dev:
    image: ${DATABASE_IMAGE:-postgres:10-alpine}
    env_file: ${DATABASE_DEV_SECRETS_FILE:-secrets/database/dev/postgres/.env}
    healthcheck:
      test: ["CMD-SHELL", "psql -h 'localhost' -U 'postgres' -c '\\l'"]
      interval: 2s
      timeout: 2s
      retries: 3
    ports:
      - "5432"
    volumes:
      - ~/.elixir/${APP_NAME}/database/dev/postgresql/data:/var/lib/postgresql/data


  database-test:
    image: ${DATABASE_IMAGE:-postgres:10-alpine}
    env_file: ${DATABASE_TEST_SECRETS_FILE:-secrets/database/test/postgres/.env}
    healthcheck:
      test: ["CMD-SHELL", "psql -h 'localhost' -U 'postgres' -c '\\l'"]
      interval: 2s
      timeout: 2s
      retries: 3
    ports:
      - "5432"
    volumes:
      - ~/.elixir/${APP_NAME}/database/test/postgresql/data:/var/lib/postgresql/data

  database-cli:
      build: ${DATABASE_CLI_IMAGE_CONTEXT:-./docker/build/pgcli}
      image: ${DATABASE_CLI_IMAGE:-exadra37/pgcli:latest}
      depends_on:
        database-dev:
          condition: service_healthy
      depends_on:
        database-test:
          condition: service_healthy
      depends_on:
        database-prod:
          condition: service_healthy

